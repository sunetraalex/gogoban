with import <nixpkgs> {}; {
  devEnv = stdenv.mkDerivation {
    name = "dev";
    buildInputs = [
      stdenv
      go_1_17
      gopls
      glibc
      glibc.static

      glfw2

      pkg-config

      libGL
      libGL.dev

      xorg.libX11
      xorg.libX11.dev
      xorg.libXcursor
      xorg.libXcursor.dev
      xorg.libXrandr
      xorg.libXinerama

      # Put everything else to make sure it worked in NixOS quickly
      xorg.libXcomposite
      xorg.libXmu
      xorg.libXxf86dga
      xorg.libFS
      xorg.libXp
      xorg.libXxf86misc
      xorg.libICE
      xorg.libXdamage
      xorg.libXpm
      xorg.libXxf86vm
      xorg.libSM
      xorg.libXdmcp
      xorg.libXpresent
      xorg.libdmx
      xorg.libWindowsWM
      xorg.libXext
      xorg.libfontenc
      xorg.libXfixes
      xorg.libXrender
      xorg.libpciaccess
      xorg.libXScrnSaver
      xorg.libXfont
      xorg.libXres
      xorg.libpthreadstubs
      xorg.libXTrap
      xorg.libXfont2
      xorg.libXt
      xorg.libxcb
      xorg.libXau
      xorg.libXft
      xorg.libXtst
      xorg.libxkbfile
      xorg.libXaw
      xorg.libXi
      xorg.libXv
      xorg.libxshmfence
      xorg.libXaw3d
      xorg.libXvMC
    ];
  };
}
