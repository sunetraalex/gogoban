package main

import (
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"

	"gogoban/board"
)

var motion = board.TilePos{}

func HandleInput() {
    // Vertical motion if not already moving horizontally
    if motion.X == 0 {
        keyDownInt := getKeysInt([]ebiten.Key{ebiten.KeyArrowDown, ebiten.KeyS})
        keyUpInt := getKeysInt([]ebiten.Key{ebiten.KeyArrowUp, ebiten.KeyW})

        motion.Y = keyDownInt - keyUpInt
    }

    // Horizontal motion if not already moving vertically
    if motion.Y == 0 {
        keyRightInt := getKeysInt([]ebiten.Key{ebiten.KeyArrowRight, ebiten.KeyD})
        keyLeftInt := getKeysInt([]ebiten.Key{ebiten.KeyArrowLeft, ebiten.KeyA})

        motion.X = keyRightInt - keyLeftInt
    }

    if motion.X != 0 || motion.Y != 0 {
        movePlayer(motion)
    }
}

// Get 1 if any button is pressed otherwise 0
func getKeysInt(keys []ebiten.Key) int {
    for _, key := range keys {
        if inpututil.IsKeyJustPressed(key) {
            return 1
        }
    }
    return 0
}

// Check the board and move the player if able to
func movePlayer(motion board.TilePos) {
    currentPos := board.TilePos{
        X: int(game.player.Pos.X),
        Y: int(game.player.Pos.Y),
    }

    desiredPos := board.TilePos{
        X: currentPos.X + motion.X,
        Y: currentPos.Y + motion.Y,
    }

    pushingPos := board.TilePos{
        X: desiredPos.X + motion.X,
        Y: desiredPos.Y + motion.Y,
    }

    playerLayer := game.board.GetLayer(board.Player)
    boxesLayer := game.board.GetLayer(board.Boxes)

    // Check if can move and push box
    collideWithWall := isCollidingWith(desiredPos, []board.LayerType{board.Walls})
    inFrontOfBox := isCollidingWith(desiredPos, []board.LayerType{board.Boxes})
    boxInFrontIsBlocked := isCollidingWith(pushingPos, []board.LayerType{board.Walls, board.Boxes})

    if !collideWithWall {
        if !inFrontOfBox {
            playerLayer.SwapTiles(currentPos, desiredPos)

        } else if inFrontOfBox && !boxInFrontIsBlocked {
            playerLayer.SwapTiles(currentPos, desiredPos)
            boxesLayer.SwapTiles(desiredPos, pushingPos)
        }
    }
}

func isCollidingWith(pos board.TilePos, layerTypes []board.LayerType) bool {
    for _, layerType := range layerTypes {
        layer := game.board.GetLayer(layerType)
        tile := layer.GetTileAt(pos)

        if tile != nil {
            return true
        }
    }
    return false
}

// Check if all boxes overlap with a target
func CheckWinCondition() bool {
    boxesLayer := game.board.GetLayer(board.Boxes)

    for _, box := range boxesLayer.Entities {
        boxTilePos := board.TilePos{
            X: int(box.Pos.X),
            Y: int(box.Pos.Y),
        }

        if !isCollidingWith(boxTilePos, []board.LayerType{board.Targets}) {
            return false
        }
    }

    return true
}
