package main

import (
    "log"

    "github.com/hajimehoshi/ebiten/v2"

    "gogoban/board"
    "gogoban/configs"
    "gogoban/entity"
    "gogoban/helpers"
)

// Level data made in layers
var boardData = []board.Layer{

    {
        Type: board.Grass,
        ImgPath: "./assets/grass.png",
        Data: [][]int{
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        },
    },

    {
        Type: board.Walls,
        ImgPath: "./assets/stone.png",
        Data: [][]int{
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 0, 0, 1, 1, 1, 1},
            {1, 0, 0, 0, 0, 0, 1, 1, 1, 1},
            {1, 0, 0, 0, 0, 0, 1, 1, 1, 1},
            {1, 1, 1, 1, 0, 0, 0, 0, 0, 1},
            {1, 1, 1, 1, 0, 1, 1, 0, 0, 1},
            {1, 1, 1, 0, 0, 0, 0, 0, 1, 1},
            {1, 1, 1, 1, 0, 0, 0, 0, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        },
    },

    {
        Type: board.Targets,
        ImgPath: "./assets/hole.png",
        Data: [][]int{
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 1, 1, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 1, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        },
    },

    {
        Type: board.Boxes,
        ImgPath: "./assets/peanut.png",
        Data: [][]int{
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 1, 0, 1, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        },
    },

    {
        Type: board.Player,
        ImgPath: "./assets/gopher.png",
        Data: [][]int{
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        },
    },

}

const (
    gameScreen = iota
    winScreen
)

var winText = entity.NewEntity(
    helpers.NewVector(2, 4),
)

// Game object
type Game struct {
    player *entity.Entity
    board *board.Board
    state int
}

var game Game

func (g *Game) Update() error {
    switch g.state {
        case gameScreen:
            HandleInput()

            if playerWon := CheckWinCondition(); playerWon {
                g.state = winScreen
            }
    }

    g.board.Update(board.Boxes)
    g.board.Update(board.Player)

    return nil
}

func (g *Game) Draw(screen *ebiten.Image) {
    switch g.state {
        case winScreen:
            defer winText.Draw(screen)
            fallthrough

        case gameScreen:
            g.board.Draw(screen, board.Grass)
            g.board.Draw(screen, board.Walls)
            g.board.Draw(screen, board.Targets)
            g.board.Draw(screen, board.Boxes)
            g.board.Draw(screen, board.Player)
    }
}

// Define internal screen size
func (g *Game) Layout(outsideWidth, outsideHeight int) (screenWidth, screenHeight int) {
    return configs.ScreenWidth, configs.ScreenHeight
}

// Initialize the game and all entities
func init() {
    gameBoard := board.NewBoard(boardData)
    playerLayer := gameBoard.GetLayer(board.Player)
    playerTile := playerLayer.Entities[0]

    game = Game{
        player: playerTile,
        board: &gameBoard,
        state: gameScreen,
    }

    winText.AssignImage("./assets/win-text.png")
}

// Game Entrypoint
func main() {
    ebiten.SetWindowSize(configs.ScreenWidth, configs.ScreenHeight)
    ebiten.SetWindowTitle("GoGoBan!")

    if err := ebiten.RunGame(&game); err != nil {
        log.Fatal(err)
    }
}
