package entity

import (
    _ "image/png"
	"log"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
)

var imageCache = make(map[string]*ebiten.Image)

// Load image or get from cache
func GetImage(path string) *ebiten.Image {
    if cachedImg := imageCache[path]; cachedImg != nil {
        return cachedImg
    }

    var err error
    var img *ebiten.Image

    img, _, err = ebitenutil.NewImageFromFile(path)
    if err != nil {
        log.Fatal(err)
    }

    imageCache[path] = img

    return img
}
