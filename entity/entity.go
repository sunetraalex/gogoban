package entity

import (
    _ "image/png"

    "github.com/hajimehoshi/ebiten/v2"

    "gogoban/configs"
    "gogoban/helpers"
)

type iEntity interface {
    AssignImage(path string)
    HandleInput()
    Update()
    Draw(screen *ebiten.Image)
}

type Entity struct {
    iEntity
    // Visual sprite
    img *ebiten.Image
    // Grid position
    Pos helpers.Vector
    // Actual screen position
    screenPos helpers.Vector
    // size of motion in screen size
    movementStep int
    // Frames to move the entity
    speed float64
    // Time passed
    delta float64
}

func (e *Entity) AssignImage(path string) {
    e.img = GetImage(path)
}

func (e *Entity) MoveTo(pos helpers.Vector) {
    e.Pos = pos
    e.delta = 0
}

func (e *Entity) CanMove() bool {
    return e.delta > e.speed
}

func (e *Entity) Update() {
    desiredPos := e.Pos.Multiply(float64(e.movementStep))

    // Snap to grid position
    if e.CanMove() {
        e.screenPos = desiredPos
        e.delta = 0

    // Smooth motion between cells
    } else {
        desiredMotion := desiredPos.Subtract(e.screenPos)

        currentProgress := e.delta / e.speed
        currentMotion := desiredMotion.Multiply(currentProgress)

        e.screenPos = e.screenPos.Add(currentMotion)
    }

    e.delta++
}

// Draw the entity on screen
func (e *Entity) Draw(screen *ebiten.Image) {
    op := &ebiten.DrawImageOptions{}
    op.GeoM.Translate(e.screenPos.X, e.screenPos.Y)

    screen.DrawImage(e.img, op)
}

// Instantiate a new Entity
func NewEntity(pos helpers.Vector) *Entity {
    newEntity := Entity{}

    newEntity.Pos = pos
    newEntity.screenPos = pos.Multiply(float64(configs.TileSize))

    newEntity.movementStep = configs.TileSize
    newEntity.speed = configs.TileSpeed

    return &newEntity
}


