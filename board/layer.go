package board

import (
    "gogoban/entity"
    "gogoban/helpers"
)

// Tile types
const (
    Grass = iota
    Walls
    Boxes
    Targets
    Player
)

type LayerType int

type TilePos struct {
    X int
    Y int
}

type Layer struct {
    // Layer type
    Type LayerType
    // Entities in the grid
    Entities []*entity.Entity
    // 2D grid of entities
    Tiles [][]*entity.Entity
    // Sprite to use when generating the layer entities
    ImgPath string
    // Data passed for configuring the layer layout
    Data [][]int
}

func (l *Layer) BuildEntities(imgPath string) {
    var tiles [][]*entity.Entity

    for y, row := range l.Data {

        var tileRow []*entity.Entity

        for x, cell := range row {
            var tile *entity.Entity
            if cell == 1 {
                tileEntityPos := helpers.NewVector(float64(x), float64(y))

                tileEntity := entity.NewEntity(tileEntityPos)
                tileEntity.AssignImage(imgPath)

                tile = tileEntity
                l.Entities = append(l.Entities, tileEntity)
            }

            tileRow = append(tileRow, tile)
        }

        tiles = append(tiles, tileRow)
    }

    l.Tiles = tiles
}

func (l *Layer) GetTileAt(pos TilePos) *entity.Entity {
    if helpers.IsIndexInRange(len(l.Tiles), pos.Y) {
        if helpers.IsIndexInRange(len(l.Tiles[pos.Y]), pos.X) {
            return l.Tiles[pos.Y][pos.X]
        }
    }
    return nil
}

func (l *Layer) SetTileAt(pos TilePos, tile *entity.Entity) {
    if tile != nil {
        tile.MoveTo(
            helpers.NewVector(float64(pos.X), float64(pos.Y)),
        )
    }

    l.Tiles[pos.Y][pos.X] = tile
}

func (l *Layer) SwapTiles(from TilePos, to TilePos) {
    fromTile := l.GetTileAt(from)
    toTile := l.GetTileAt(to)

    l.SetTileAt(from, toTile)
    l.SetTileAt(to, fromTile)

    fromTile = l.GetTileAt(from)
    toTile = l.GetTileAt(to)
}
