package board

import (
    "gogoban/entity"

    "github.com/hajimehoshi/ebiten/v2"
)

// Game board for holding entities in grids and interactiong with them
type Board struct {
    layers map[LayerType]Layer
}

func (b *Board) AddLayer(layer Layer) {
    layer.BuildEntities(layer.ImgPath)
    b.layers[layer.Type] = layer
}

func (b *Board) GetLayer(layerType LayerType) Layer {
    return b.layers[layerType]
}

func (b *Board) GetTile(layerType LayerType, pos TilePos) *entity.Entity {
    layer := b.GetLayer(layerType)
    return layer.GetTileAt(pos)
}

func (b *Board) GetTilesPos(layerType LayerType) []*entity.Entity {
    layer := b.GetLayer(layerType)
    return layer.Entities
}

func (b *Board) Update(layerType LayerType) {
    layer := b.GetLayer(layerType)

    for _, tile := range layer.Entities {
        tile.Update()
    }
}

func (b *Board) Draw(screen *ebiten.Image, layerType LayerType) {
    layer := b.GetLayer(layerType)

    for _, tile := range layer.Entities {
        tile.Draw(screen)
    }
}

func NewBoard(layers []Layer) Board {
    newBoard := Board{layers: make(map[LayerType]Layer)}
    for _, layer := range layers {
        newBoard.AddLayer(layer)
    }
    return newBoard
}

