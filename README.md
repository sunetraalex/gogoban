<img src="assets/logo.png" alt="Logo" width="100"/>

GoGoBan!
========

A basic Sokoban clone made to learn [Go](https://go.dev/) with [Ebiten](https://ebitengine.org/).

Non-Features
------------

* No Start screen!
* No Levels!
* No Undo!
* No Sounds!

Controls
--------

Use Arrows or WASD to move.

Push all peanuts in the holes.

> **Enjoy!**

### Credits

> Code released under the [MIT License](LICENSE)

> <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />Graphical assets are licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.

