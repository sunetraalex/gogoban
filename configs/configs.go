package configs

// Shared configuration

const (
    TileCount = 10
    TileSize = 96
    TileSpeed = 20.0
)

const (
    ScreenWidth = TileSize * TileCount
    ScreenHeight = TileSize * TileCount
)
