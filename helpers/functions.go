package helpers

func Lerp(startValue, endValue, amount float64) float64 {
    return (startValue + (endValue - startValue) * amount)
}

func IsIndexInRange(length, i int) bool {
    return i >= 0 && i < length
}
