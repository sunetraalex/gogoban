package helpers

type Vector struct {
    X, Y float64
}

func (v Vector) Add(v2 Vector) Vector {
    v.X += v2.X
    v.Y += v2.Y
    return v
}

func (v Vector) Subtract(v2 Vector) Vector {
    v.X -= v2.X
    v.Y -= v2.Y
    return v
}

func (v Vector) Multiply(amount float64) Vector {
    v.X *= amount
    v.Y *= amount
    return v
}

func NewVector(x, y float64) Vector {
    return Vector{x, y}
}
